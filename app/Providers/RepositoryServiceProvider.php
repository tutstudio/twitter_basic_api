<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoriesServiceProvider
 * @package Nubelo\Repositories
 */
class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Binding
     */
    public function register()
    {
        $this->app->bind(
            'Twitter\Domain\Twit\TwitRepositoryInterface',
            'Twitter\Infrastructure\Integration\TwitterApi\TwitRepository'
        );
    }
}