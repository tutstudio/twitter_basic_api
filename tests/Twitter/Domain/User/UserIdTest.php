<?php

class UserIdTest extends TestCase
{
    public function testUserIdShouldGetId()
    {
        $id = 1;

        $userId = new \Twitter\Domain\User\UserId($id);

        $this->assertEquals($id, $userId->id());
    }
}