<?php

class UserTest extends TestCase
{
    protected $twitsCollection;

    public function setUp()
    {
        $twit1 = new \Twitter\Domain\Twit\Twit();
        $twit1->setId(new \Twitter\Domain\Twit\TwitId(1))
            ->setText('twit 1')
            ->setCreatedAt('12 enero 2016');
        $twit2 = new \Twitter\Domain\Twit\Twit();
        $twit2->setId(new \Twitter\Domain\Twit\TwitId(2))
            ->setText('twit 2')
            ->setCreatedAt('13 enero 2016');
        $twit3 = new \Twitter\Domain\Twit\Twit();
        $twit3->setId(new \Twitter\Domain\Twit\TwitId(3))
            ->setText('twit 3')
            ->setCreatedAt('13 enero 2016');

        $this->twitsCollection = [$twit1, $twit2, $twit3];
    }

    public function testUserShouldSetAndGetId()
    {
        $id = new \Twitter\Domain\User\UserId(1);

        $user = new \Twitter\Domain\User\User();
        $user->setId($id);

        $this->assertEquals($id, $user->getId());
    }

    public function testUserShouldSetAndGetName()
    {
        $name = 'user name';

        $user = new \Twitter\Domain\User\User();
        $user->setName($name);

        $this->assertEquals($name, $user->getName());
    }

    public function testUserShouldSetAndGetUserName()
    {
        $username = 'username';

        $user = new \Twitter\Domain\User\User();
        $user->setUsername($username);

        $this->assertEquals($username, $user->getUsername());
    }

    public function testUserShouldAddTwit()
    {
        $user = new \Twitter\Domain\User\User();
        $user->addTwits($this->twitsCollection[0]);

        $this->assertEquals([$this->twitsCollection[0]], $user->getTwits());
    }

    public function testUserShouldRemove()
    {
        $user = new \Twitter\Domain\User\User();
        $user->addTwits($this->twitsCollection[0])
            ->addTwits($this->twitsCollection[1]);

        $user->removeTwit($this->twitsCollection[0]);

        $this->assertEquals([$this->twitsCollection[1]], $user->getTwits());
    }

    public function testUserShouldGetTwits()
    {
        $user = new \Twitter\Domain\User\User();
        $user->addTwits($this->twitsCollection[0])
            ->addTwits($this->twitsCollection[1])
            ->addTwits($this->twitsCollection[2]);

        $this->assertEquals($this->twitsCollection, $user->getTwits());
    }
}