<?php

class TwitIdTest extends TestCase
{
    public function testTwitIdShouldGetId()
    {
        $id = 1;

        $twitId = new \Twitter\Domain\Twit\TwitId($id);

        $this->assertEquals($id, $twitId->id());
    }
}