<?php

class TwitTest extends TestCase
{
    public function testTwitShouldSetAndGetId()
    {
        $id = new \Twitter\Domain\Twit\TwitId(1);

        $twit = new \Twitter\Domain\Twit\Twit();
        $twit->setId($id);

        $this->assertEquals($id, $twit->getId());
    }

    public function testTwitShouldSetAndGetText()
    {
        $text = 'twit 1';
        $twit = new \Twitter\Domain\Twit\Twit();
        $twit->setText($text);

        $this->assertEquals($text, $twit->getText());
    }

    public function testTwitShouldSetAndGetCreatedAt()
    {
        $date = '25 julio 2016';

        $twit = new \Twitter\Domain\Twit\Twit();
        $twit->setCreatedAt($date);

        $this->assertEquals($date, $twit->getCreatedAt());
    }

    public function testTwitShouldSetAndGetUser()
    {
        $user = new \Twitter\Domain\User\User();

        $twit = new \Twitter\Domain\Twit\Twit();
        $twit->setUser($user);

        $this->assertEquals($user, $twit->getUser());
    }
}