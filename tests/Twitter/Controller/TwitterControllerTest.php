<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TwitterControllerTest extends TestCase
{
    public function testGetUserTwitsShouldReturnResponseWhenUserExists()
    {
        $this->visit('/api/twits/jordievole')
            ->see('data')
            ->dontSee('errors');
    }

    public function testGetUserTwitsShouldReturnResponseWhenUserDontExists()
    {
        $this->visit('/api/twits/123')
            ->see('errors')
            ->dontSee('data');
    }
}