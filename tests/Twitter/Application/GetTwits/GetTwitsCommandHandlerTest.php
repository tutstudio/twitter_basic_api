<?php

use Twitter\Application\GetTwits\GetTwitsArgumentsValidator;
use Twitter\Application\GetTwits\GetTwitsCommandHandler;
use Twitter\Infrastructure\Integration\TwitterApi\TwitRepositoryInMemory;

class GetTwitsCommandHandlerTest extends TestCase
{
    public function testHandlerMethodShouldReturnGetTwitsPresenterInstance()
    {
        $repository = new TwitRepositoryInMemory();
        $handler = new GetTwitsCommandHandler($repository, new GetTwitsArgumentsValidator());
        $command = new \Twitter\Application\GetTwits\GetTwitsCommand('username');

        $presenter = $handler->handle($command);

        $this->assertInstanceOf('Twitter\Application\GetTwits\GetTwitsApiPresenter', $presenter);
    }
}