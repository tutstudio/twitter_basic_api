<?php

use Twitter\Application\GetTwits\GetTwitsArgumentsValidator;

class GetTwitsArgumentsValidatorTest extends TestCase
{
    /**
     * @expectedException Twitter\Application\GetTwits\Exception\GetTwitsMissingArguments
     */
    public function testGetTwitsArgumentsValidatorShouldThrowMissingException()
    {
        $validator = new GetTwitsArgumentsValidator();

        $validator->validate(null);
    }

    /**
     * @expectedException Twitter\Application\GetTwits\Exception\GetTwitsInvalidArguments
     */
    public function testGetTwitsArgumentsValidatorShouldThrowInvalidException()
    {
        $validator = new GetTwitsArgumentsValidator();

        $validator->validate(12345);
    }
}