<?php

use Twitter\Application\GetTwits\GetTwitsCommand;

class GetTwitsCommandTest extends TestCase
{
    public function testGetTwitsCommandShouldGetUserNameArgument()
    {
        $username = 'username';
        $command = new GetTwitsCommand($username);

        $this->assertEquals($username, $command->username());
    }
}