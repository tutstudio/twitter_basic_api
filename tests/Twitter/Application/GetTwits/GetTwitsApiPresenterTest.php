<?php

use Twitter\Application\GetTwits\GetTwitsApiPresenter;
use Twitter\Domain\Twit\Twit;
use Twitter\Domain\Twit\TwitId;

class GetTwitsApiPresenterTest extends TestCase
{
    protected $array = [
        0 => [
            'id' => 1,
            'text' => 'twit 1',
            'createdAt' => '25 de enero de 2016'
        ],
        1 => [
            'id' => 2,
            'text' => 'twit 2',
            'createdAt' => '25 de enero de 2016'
        ]
    ];

    protected $twits;

    public function setUp()
    {
        $twit1 = new Twit();
        $twit1->setId( new TwitId(1))
            ->setText('twit 1')
            ->setCreatedAt("25 de enero de 2016");
        $twit2 = new Twit();
        $twit2->setId( new TwitId(2))
            ->setText('twit 2')
            ->setCreatedAt("25 de enero de 2016");

        $this->twits = [$twit1, $twit2];
    }

    public function testGetTwitsApiPresenterShouldConvertTwitObjectToArray()
    {
        $presenter = new GetTwitsApiPresenter($this->twits);

        $this->assertEquals($this->array, $presenter->toArray());
    }

    public function testGetTwitsApiPresenterShouldReturnTheCountOfTwits()
    {
        $presenter = new GetTwitsApiPresenter($this->twits);

        $this->assertEquals(count($this->array), $presenter->count());
    }
}