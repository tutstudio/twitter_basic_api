# Twitter Api

Esta aplicación Rest es muy básica con una sola entrada **/api/twits/{username}**, permite acceder a los 10 últimos twits de un usuario.
Esta desarrolla siguiendo la arquitectura hexagonal bajo el paradigma DDD, el patrón del commandbus es el encargado de aislar
las acciones en comandos.

el cache esta implementado a nivel de interfaces ya que se usa la gestión de cache del framework, una manera óptima sera implementar
un sistema de gestión de cache en la aplicación y de esa manera se desacopla lo mejor posible del framework.

Las excepciones son básicas para prevenir el parámetro de la llamada, y el fallo general de la interacción con twitter, queda pendiente
 a mejorar la gestión de error twitter, como la conexión, nombre de usuario que no existe o cuando hay otro fallo etc...
 
El flujo de la llamada **/twits/{username}** es el siguien:

- llega la la llamada al router que la redirige al controlador TwitterController.
- el método del controlador ejecuta el comando GetTwitsCommand construido con el parámetro username.
- el gestor del comando recibe el parámetro, lo valida, en caso de que no sea valido se lanza la excepción que sera tratada por el handler del framework para presentar los errores en json (App/Exception/handler).
- despues pasa el parámetro al método del repositorio encargado de buscar los datos, este de su lado llama al proveedor de twitter en caso de que algo va mal lanza una excepción con error 500, si todo va bien
recoge los datos y devuelve un array de la entidad twit llenada.
- el gestor de comando recibe la colección y la pasa a su presentador que en este caso tiene un método que convierte la colección a un array (extinción: añadir métodos para devolver otros formatos como json, xml...)
- el controlador recibe el presentador, y de su parte devuelve response json pasándole los datos en formato array.
 


## Requerimientos

- composer
- vagrant
- virtualBox

## instalación

Después de descargar el contenido sigue las pautas siguientes:

- copia el archivo .env.example a .env.

- modifica las claves de acceso a la api twitter

- composer install

- linux: **php vendor/bin/homestead make**
- windows: **vendor\\bin\\homestead make**

En la raíz del proyecto se crea el archivo homestead.yml, se puede modificar lo siguiente:

- ip: (es la ip de la aplicación)

- sites:
 - map: twitter.api (enlace de la api, para que funcione debe añadirlo a su archivo hosts apuntado a la ip modificada antes)
   to: "/home/vagrant/twitterapi/public"

- databases:
 - twitter (base de datos que se crea al iniciar vagrant)

Ya tenemos la aplicación lista para su uso, para lanzar los tests utiliza el comando:
- linux: **php vendor/bin/phpunit**
- windows: **vendor\\bin\\phpunit**

pd: los test unitarios cubren la capa de aplicación y de dominio.

## A mejorar

- Implementar un gestor de cache de la api
- Añadir oAuth para acceder a la api
- Gestión de errores api twitter
- precommit
...