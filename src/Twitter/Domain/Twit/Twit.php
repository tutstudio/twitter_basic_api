<?php
/**
 * Created by PhpStorm.
 * User: amine.hajji
 * Date: 11/7/16
 * Time: 16:55
 */

namespace Twitter\Domain\Twit;

use Twitter\Domain\User\User;

class Twit
{
    protected $id;
    protected $text;
    protected $user;
    protected $createdAt;

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $createdAt
     * @return Twit
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param TwitId $id
     * @return Twit
     */
    public function setId(TwitId $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return Twit
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Twit
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }


}