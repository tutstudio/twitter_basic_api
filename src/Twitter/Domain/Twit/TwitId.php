<?php

namespace Twitter\Domain\Twit;

class TwitId
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function id()
    {
        return $this->id;
    }
}