<?php
/**
 * Created by PhpStorm.
 * User: Amine
 * Date: 12/07/2016
 * Time: 0:03
 */

namespace Twitter\Domain\Twit;


interface TwitApiPresenter
{
    public function convert(Twit $twit);
}