<?php
/**
 * Created by PhpStorm.
 * User: amine.hajji
 * Date: 26/7/16
 * Time: 13:07
 */

namespace Twitter\Domain\Twit;

use ArrayIterator;
use IteratorAggregate;

class TwitCollection implements IteratorAggregate
{
    protected $twits;

    public function __construct($twits)
    {
        $this->twits = $twits;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->twits);
    }
}