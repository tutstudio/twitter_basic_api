<?php

namespace Twitter\Domain\Twit;

interface TwitRepositoryInterface
{
    public function findTwitByUsername($username, $limit = null);
}