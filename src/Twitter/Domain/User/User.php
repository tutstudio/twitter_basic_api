<?php

namespace Twitter\Domain\User;

use Doctrine\Common\Collections\ArrayCollection;
use Twitter\Domain\Twit\Twit;

class User
{
    protected $id;
    protected $name;
    protected $username;
    protected $twits;

    /**
     * @return UserId
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param UserId $id
     * @return User
     */
    public function setId(UserId $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return array
     */
    public function getTwits()
    {
        return $this->twits;
    }

    /**
     * @param Twit $twits
     * @return User
     */
    public function addTwits(Twit $twits)
    {
        $this->twits[] = $twits;

        return $this;
    }

    /**
     * @param Twit $twits
     * @return $this
     */
    public function removeTwit(Twit $twits)
    {
        foreach ($this->twits as $index => $value) {
            if ($value == $twits) {
                unset($this->twits[$index]);
                $this->twits = array_values($this->twits);
            }
        }

        return $this;
    }
}