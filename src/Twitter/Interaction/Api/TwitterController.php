<?php

namespace Twitter\Interaction\Api;

use Illuminate\Http\Request;
use Twitter\Application\GetTwits\GetTwitsApiPresenter;
use Twitter\Application\GetTwits\GetTwitsCommand;

class TwitterController extends ApiController
{
    const GET_TWITS_CACHE_MINUTES = 1;

    protected $module = 'twitter';

    public function getUserTwits($username, Request $request)
    {
        $data = \Cache::remember('users', self::GET_TWITS_CACHE_MINUTES, function () use($username) {
            $command = new GetTwitsCommand($username);
            /** @var GetTwitsApiPresenter $twits */
            $twits = $this->commander->execute($command);

            return $twits->toArray();
        });

        return $this->response($data);
    }
}