<?php

namespace Twitter\Interaction\Api;

use App\Http\Controllers\Controller;
use Twitter\Infrastructure\Vendor\CommandBus\CommandBus;

class ApiController extends Controller
{
    protected $commander;
    protected $module;

    public function __construct(CommandBus $commandBus)
    {
        $this->commander = $commandBus;
    }

    public function response($data)
    {
        return response()->json([
            'module' => $this->module,
            'status' => true,
            'data' => $data
            ],
            200,
            [],
            JSON_UNESCAPED_SLASHES|
            JSON_UNESCAPED_UNICODE
        );
    }
}