<?php

namespace Twitter\Infrastructure\Vendor\CommandBus;

use Exception;

class CommandTranslator
{
    /**
     * get command handler name class
     * @param $command
     * @return mixed
     * @throws Exception
     */
    public function toCommandHandler($command)
    {
        $handler = str_replace('Command', 'CommandHandler', get_class($command));
        if (!class_exists($handler)) {
            $msg = "Command handler '$handler' does not exist.";

            throw new Exception($msg);
        }

        return $handler;
    }
}