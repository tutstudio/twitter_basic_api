<?php

namespace Twitter\Infrastructure\Vendor\CommandBus;

interface CommandHandlerInterface
{
    /**
     * Handle the command
     * @param Object $command
     * @return mixed
     */
    public function handle($command);
}