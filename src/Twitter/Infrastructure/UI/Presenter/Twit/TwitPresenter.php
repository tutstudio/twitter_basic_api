<?php

namespace Twitter\Infrastructure\UI\Presenter\Twit;

use Twitter\Domain\Twit\Twit;
use Twitter\Domain\Twit\TwitApiPresenter;

class TwitPresenter implements TwitApiPresenter
{
    /**
     * @param Twit $twit
     * @return array
     */
    public function convert(Twit $twit)
    {
        return [
            'id' => $twit->getId()->id(),
            'text' => $twit->getText(),
            'createdAt' => $twit->getCreatedAt()
        ];
    }
}