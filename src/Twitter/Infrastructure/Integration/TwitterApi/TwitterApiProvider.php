<?php

namespace Twitter\Infrastructure\Integration\TwitterApi;

class TwitterApiProvider
{
    public function getUserTimeline($username, $limit)
    {
        return $twits =  \Twitter::getUserTimeline(array(
            'screen_name' => $username,
            'count' => $limit
        ));
    }
}