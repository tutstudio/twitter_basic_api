<?php
/**
 * Created by PhpStorm.
 * User: Amine
 * Date: 11/07/2016
 * Time: 23:41
 */

namespace Twitter\Infrastructure\Integration\TwitterApi;


use Twitter\Domain\Twit\Twit;
use Twitter\Domain\Twit\TwitId;
use Twitter\Domain\Twit\TwitRepositoryInterface;
use Twitter\Domain\User\User;
use Twitter\Domain\User\UserId;

class TwitRepositoryInMemory implements TwitRepositoryInterface
{
    protected $user = [
        'id' => 1,
        'name' => 'user name',
        'username' => 'username'
    ];

    protected $twits = [
        0 => [
            'id' => 1,
            'text' => 'twit 1',
            'created_at' => '25 de agosto de 2016'
        ],
        1 => [
            'id' => 2,
            'text' => 'twit 2',
            'created_at' => '25 de agosto de 2016'
        ],
        2 => [
            'id' => 3,
            'text' => 'twit 3',
            'created_at' => '25 de agosto de 2016'
        ],
    ];

    public function findTwitByUsername($username, $limit = null)
    {
        $twits = [];

        $user = new User();
        $user->setId(new UserId($this->user['id']))
            ->setName($this->user['name'])
            ->setUsername($this->user['username']);

        foreach ($this->twits as $twit) {
            $userTwit = new Twit();
            $userTwit->setId(new TwitId($twit['id']))
                ->setText($twit['text'])
                ->setCreatedAt($twit['created_at'])
                ->setUser($user);

            $twits[] = $userTwit;
        }

        return $twits;
    }
}