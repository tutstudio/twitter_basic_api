<?php

namespace Twitter\Infrastructure\Integration\TwitterApi;

use Twitter;
use Twitter\Domain\Twit\TwitCollection;
use Twitter\Domain\Twit\TwitRepositoryInterface;

class TwitRepository implements TwitRepositoryInterface
{
    const TWITS_DEFAULT_LIMIT = 10;

    protected $twitter;

    public function __construct(TwitterApiProvider $twitter)
    {
        $this->twitter = $twitter;
    }

    public function findTwitByUsername($username, $limit = null)
    {
        if (null === $limit) {
            $limit = self::TWITS_DEFAULT_LIMIT;
        }

        try {
            $twits = $this->twitter->getUserTimeline($username, $limit);
        } catch (\Exception $e) {
            throw new TwitterApiProviderException('internal error, contact support!');
        }

        return new TwitCollection($this->toEntity($twits));
    }

    private function toEntity($twits)
    {
        $userTwits = [];

        foreach ($twits as $twit) {
            $user = new Twitter\Domain\User\User();
            $user->setId(new Twitter\Domain\User\UserId($twit->user->id))
                ->setName($twit->user->name)
                ->setUsername($twit->user->screen_name);

            $userTwit = new Twitter\Domain\Twit\Twit();
            $userTwit->setId(new Twitter\Domain\Twit\TwitId($twit->id))
                ->setText($twit->text)
                ->setUser($user)
                ->setCreatedAt($twit->created_at);

            $userTwits[] = $userTwit;
        }

        return $userTwits;
    }
}