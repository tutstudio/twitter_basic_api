<?php

namespace Twitter\Infrastructure\Integration\TwitterApi;

use Twitter\Application\ApiExceptionHandler;

class TwitterApiProviderException extends ApiExceptionHandler
{
    protected $stringError = 'ERROR_INTERNAL_500';

    public function __construct($message)
    {
        parent::__construct($message, 500);
    }
}