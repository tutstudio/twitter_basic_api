<?php

namespace Twitter\Application\GetTwits;

class GetTwitsCommand
{
    protected $username;

    public function __construct($username)
    {
        $this->username = $username;
    }

    public function username()
    {
        return $this->username;
    }
}