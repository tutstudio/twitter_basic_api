<?php

namespace Twitter\Application\GetTwits\Exception;

use Twitter\Application\ApiExceptionHandler;

class GetTwitsInvalidArguments extends ApiExceptionHandler
{
    protected $stringError = 'ERROR_INVALID_ARGUMENTS';

    public function __construct($message)
    {
        parent::__construct($message, 400);
    }
}