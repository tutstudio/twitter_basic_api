<?php

namespace Twitter\Application\GetTwits\Exception;

use Twitter\Application\ApiExceptionHandler;

class GetTwitsMissingArguments extends ApiExceptionHandler
{
    protected $stringError = 'ERROR_MISSING_ARGUMENTS';

    public function __construct($message)
    {
        parent::__construct($message, 400);
    }
}