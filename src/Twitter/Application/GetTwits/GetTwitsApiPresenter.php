<?php
/**
 * Created by PhpStorm.
 * User: Amine
 * Date: 12/07/2016
 * Time: 0:05
 */

namespace Twitter\Application\GetTwits;

use Twitter\Domain\Twit\Twit;
use Twitter\Domain\Twit\TwitApiPresenter;

class GetTwitsApiPresenter
{
    protected $twits;

    public function __construct($twits)
    {
        $this->twits = $twits;
    }

    public function toArray()
    {
        $collection = [];

        foreach ($this->twits as $twit) {
            $collection[] = $this->convert($twit);
        }

        return $collection;
    }

    public function count()
    {
        return count($this->twits);
    }

    public function convert(Twit $twit)
    {
        return [
            'id' => $twit->getId()->id(),
            'text' => $twit->getText(),
            'createdAt' => $twit->getCreatedAt()
        ];
    }
}