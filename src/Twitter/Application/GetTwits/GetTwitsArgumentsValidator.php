<?php

namespace Twitter\Application\GetTwits;

use Twitter\Application\GetTwits\Exception\GetTwitsMissingArguments;
use Twitter\Application\GetTwits\Exception\GetTwitsInvalidArguments;

class GetTwitsArgumentsValidator
{
    /**
     * @param $username
     * @throws GetTwitsInvalidArguments
     * @throws GetTwitsMissingArguments
     */
    public function validate($username)
    {
        if (null === $username) {
            throw new GetTwitsMissingArguments('missing username argument!');
        }

        if (!is_string($username)) {
            throw new GetTwitsInvalidArguments('username must be a string!');
        }
    }
}