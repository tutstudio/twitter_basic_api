<?php
/**
 * Created by PhpStorm.
 * User: Amine
 * Date: 12/07/2016
 * Time: 0:20
 */

namespace Twitter\Application\GetTwits;

use Twitter\Domain\Twit\TwitRepositoryInterface;

class GetTwitsCommandHandler
{
    const TWITS_LIMIT = 10;

    protected $twitRepository;
    protected $validator;

    public function __construct(TwitRepositoryInterface $interface, GetTwitsArgumentsValidator $validator)
    {
        $this->twitRepository = $interface;
        $this->validator = $validator;
    }

    public function handle(GetTwitsCommand $command)
    {
        $this->validator->validate($command->username());

        $twits = $this->twitRepository->findTwitByUsername(
            $command->username(),
            self::TWITS_LIMIT
        );

        return new GetTwitsApiPresenter($twits);
    }
}