<?php
/**
 * Created by PhpStorm.
 * User: amine.hajji
 * Date: 12/7/16
 * Time: 13:16
 */

namespace Twitter\Application;


class ApiExceptionPresenter
{

    public static function render($exception)
    {
        return $data = [
            'status' => false,
            'errors' => [
                'message' => $exception->getMessage(),
                'code' => $exception->getStringCode()
            ]
        ];
    }
}