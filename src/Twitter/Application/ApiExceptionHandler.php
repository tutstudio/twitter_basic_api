<?php

namespace Twitter\Application;

class ApiExceptionHandler extends \Exception
{
    protected $stringError;

    public function getStringCode()
    {
        return $this->stringError;
    }
}